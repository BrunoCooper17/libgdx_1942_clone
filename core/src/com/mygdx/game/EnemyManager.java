package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

import java.util.Scanner;

public class EnemyManager extends GameActor {
    private final BulletManager _BulletManager = new BulletManager(100);

    private final Array<Enemy> _Enemies;
    private final Array<String> _LevelPattern;
    private int _CurrentPatternLine = 0;
    private int _FramesToWait = 0;

    public EnemyManager(int EnemyPool) {
        _Enemies = new Array<>(EnemyPool);
        for (int Idx = 0; Idx < EnemyPool; ++Idx) {
            _Enemies.add(new Enemy());
        }

        _LevelPattern = new Array<>();
    }

    public void Init(String PatternsFile) {
        _LevelPattern.clear();

        FileHandle File = Gdx.files.internal(PatternsFile);
        Scanner Reader = new Scanner(File.read());
        while (Reader.hasNextLine()) {
            _LevelPattern.add(Reader.nextLine());
        }

        _CurrentPatternLine = 0;
        _FramesToWait = 0;
    }

    private Enemy GetEnemy() {
        for (Enemy enemy : _Enemies) {
            if (!enemy.IsAlive()) {
                return enemy;
            }
        }

        return null;
    }

    void ProcessFramePattern() {
        while (_FramesToWait <= 0) {
            Scanner Pattern = new Scanner(_LevelPattern.get(_CurrentPatternLine));

            // Enemy Type
            EEnemyData EnemyType = EEnemyData.valueOf(Pattern.next());
            // Route Pattern
            ERouteData RouteType = ERouteData.valueOf(Pattern.next());
            // Additional Enemies to Spawn
            int AdditionalEnemiesToSpawn = Pattern.nextInt();
            // IF Additional Enemies to Spawn > 0, Secondary enemy type
            EEnemyData SecondaryEnemy = EEnemyData.INVALID;
            if (AdditionalEnemiesToSpawn > 0) {
                SecondaryEnemy = EEnemyData.valueOf(Pattern.next());
            }
            // Frames to wait for next Pattern
            _FramesToWait = Pattern.nextInt();

            Enemy enemy = GetEnemy();
            if (enemy != null) {
                enemy.Init(EnemyType, RouteType, 0);
            }

            while (AdditionalEnemiesToSpawn > 0) {
                Enemy AdditionalEnemy = GetEnemy();
                if (AdditionalEnemy != null) {
                    AdditionalEnemy.Init(SecondaryEnemy, RouteType, AdditionalEnemiesToSpawn * 16);
                }

                --AdditionalEnemiesToSpawn;
            }

            ++_CurrentPatternLine;
            if (_CurrentPatternLine >= _LevelPattern.size) {
                _CurrentPatternLine = 0;
            }
        }
    }

    @Override
    public void Update(float DeltaTime) {
        --_FramesToWait;
        if (_FramesToWait <= 0) {
            ProcessFramePattern();
        }

        for (Enemy enemy : _Enemies) {
            if (enemy.IsAlive()) {
                enemy.Update(DeltaTime);
            }
        }
    }

    @Override
    public void Render(SpriteBatch Batch) {
        for (Enemy enemy : _Enemies) {
            if (enemy.IsAlive()) {
                enemy.Render(Batch);
            }
        }
    }
}
