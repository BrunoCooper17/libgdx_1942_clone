package com.mygdx.game;

public enum EBulletData {
    BULLET_1(150, 1, 0, 0),
    BULLET_2(150, 2, 0, 1),
    BULLET_3(150, 3, 0, 2),
    BULLET_4(150, 4, 0, 3),

    MISSILE_1(200, 2, 1, 0),
    MISSILE_2(200, 4, 1, 1),
    MISSILE_3(200, 6, 1, 2),
    MISSILE_4(200, 8, 1, 3);

    private final BulletData _BulletData;

    EBulletData(float MaxSpeed, int Damage, int SpriteRow, int SpriteCol) {
        _BulletData = new BulletData(MaxSpeed, Damage, SpriteRow, SpriteCol);
    }

    public BulletData Get() { return _BulletData; }
}

class BulletData extends SpriteData {
    private final float _MaxSpeed;
    private final int _Damage;

    BulletData(float MaxSpeed, int Damage, int SpriteRow, int SpriteCol) {
        super(SpriteRow, SpriteCol);

        _MaxSpeed = MaxSpeed;
        _Damage = Damage;
    }

    public float GetMaxSpeed() { return _MaxSpeed; }
    public int GetDamage() { return _Damage; }
}
