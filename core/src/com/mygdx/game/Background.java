package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Background extends GameActor {
    private final Sprite _Img;
    private final float _Speed;

    Background(String BackgroundLocation, float Speed) {
        Texture BgTexture = TextureManager.Get().GetTexture(BackgroundLocation);
        BgTexture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);

        _Img = new Sprite(BgTexture);
        _Img.setColor(1f, 1f, 1f, 0.525f);
        _Img.setOrigin(_Img.getWidth() * 0.5f, 0);
        _Img.setOriginBasedPosition(0, -90);

        _Speed = Speed;
    }

    @Override
    public void Update(float DeltaTime) {
        float FrameDistance = _Speed * DeltaTime / _Img.getHeight();
        _Img.scroll(0, -FrameDistance);
    }

    @Override
    public void Render(SpriteBatch Batch) {
        _Img.draw(Batch);
    }
}
