package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Enemy extends GameActor {
    private final Sprite _EnemySprite;
    private final int SpriteSize = 32;

    private int _LifeLeft;

    private RouteData _RouteData;
    private float _DistanceMoved = 0.f;

    private final Vector2 _MoveDirection = new Vector2();

    Enemy() {
        _EnemySprite = new Sprite(TextureManager.Get().
                GetTexture("kenney_pixel-shmup/Tilemap/ships_packed.png"),
                0 ,0, SpriteSize, SpriteSize);
    }

    public void Init(EEnemyData EnemyType, ERouteData Route, int DeltaOffset) {
        EnemyData EnemyData = EnemyType.Get();
        _EnemySprite.setRegion(
                EnemyData.GetSpriteCol() * SpriteSize, EnemyData.GetSpriteRow() * SpriteSize,
                SpriteSize, SpriteSize);
        _EnemySprite.setOriginCenter();

        _LifeLeft = EnemyData.GetLifePoints();

        _RouteData = Route.Get();
        _DistanceMoved = -DeltaOffset;
    }

    boolean IsAlive() { return _LifeLeft > 0; }

    @Override
    public void Update(float DeltaTime) {
        final float MaxSpeed = 150.f * DeltaTime;

        if (!IsAlive()) {
            return;
        }

        Vector2 Position = _Transform.getPosition();
        _MoveDirection.set(-Position.x, -Position.y);

        _DistanceMoved += MaxSpeed;
        Position.set(_RouteData.GetPointAtDistance(_DistanceMoved));

        _MoveDirection.add(Position);
        _MoveDirection.nor();

        _Transform.setPosition(Position);

        if (_DistanceMoved > _RouteData.GetRouteDistance()) {
            // Kill Enemy (So it's available again)
            _LifeLeft = 0;
        }
    }

    @Override
    public void Render(SpriteBatch Batch) {
        Vector2 Position = _Transform.getPosition();

        _EnemySprite.setOriginBasedPosition(Position.x, Position.y);
        if (!_MoveDirection.isZero()) {
            _EnemySprite.setRotation(_MoveDirection.angleDeg() - 90);
        }

        _EnemySprite.draw(Batch);
    }
}
