package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.util.Scanner;

public enum ERouteData {
    STANDARD("EnemyRoutes/01.txt"),
    GOOGLES("EnemyRoutes/02.txt"),
    HIT_AND_RUN(""),
    IN_OUT("EnemyRoutes/03.txt"),
    OUT_IN("");

    private final RouteData _RouteData;

    private ERouteData(String RouteFilePath) {
        _RouteData = new RouteData(RouteFilePath);
    }

    public RouteData Get() { return _RouteData; }
}

class RouteData {
    private final Array<Vector2> _RoutePoints;
    private final Array<Float> _DistanceAtPoint;

    private final Vector2 _PointAtDistance = new Vector2();

    RouteData(String FileLocation) {
        _RoutePoints = new Array<>();
        _DistanceAtPoint = new Array<>();

        if (FileLocation.isEmpty()) {
            return;
        }

        FileHandle File = Gdx.files.internal(FileLocation);
        Scanner Reader = new Scanner(File.read());

        float DistanceSoFar = 0.f;
        _DistanceAtPoint.add(0.f);

        while (Reader.hasNextInt()) {
            int X = Reader.nextInt();
            int Y = Reader.nextInt();

            if (_RoutePoints.size > 0) {
                DistanceSoFar += _RoutePoints.peek().dst(X, Y);
                _DistanceAtPoint.add(DistanceSoFar);
            }

            _RoutePoints.add(new Vector2(X, Y));
        }
    }

    public Vector2 GetPointAtIndex(int Index) { return _RoutePoints.get(Index); }

    public Vector2 GetPointAtDistance(float Distance) {
        Vector2 PointA;
        Vector2 PointB;

        final boolean bAtBegin = Distance <= 0.f;
        final boolean bAtEnd = Distance > _DistanceAtPoint.peek();

        if (bAtBegin || bAtEnd) {
            if (bAtBegin) {
                PointA = _RoutePoints.first();
                PointB = _RoutePoints.get(1);
            } else {
                PointA = _RoutePoints.peek();
                PointB = _RoutePoints.get(_RoutePoints.size-2);
            }

            // Direction
            _PointAtDistance.set(PointB.x - PointA.x, PointB.y - PointA.y);
            _PointAtDistance.nor();

            // Distance with direction
            _PointAtDistance.set(_PointAtDistance.x * Distance + PointA.x,
                    _PointAtDistance.y * Distance + PointA.y);
        } else {
            for (int Idx = 1; Idx < _DistanceAtPoint.size; ++Idx) {
                if (Distance <= _DistanceAtPoint.get(Idx)) {
                    PointA = _RoutePoints.get(Idx - 1);
                    PointB = _RoutePoints.get(Idx);

                    final float Progress = MathUtils.map(
                            _DistanceAtPoint.get(Idx - 1), _DistanceAtPoint.get(Idx),
                            0, 1,
                            Distance);
                    _PointAtDistance.set(
                            MathUtils.lerp(PointA.x, PointB.x, Progress),
                            MathUtils.lerp(PointA.y, PointB.y, Progress));

                    break;
                }
            }
        }

        return _PointAtDistance;
    }

    public float GetRouteDistance() { return _DistanceAtPoint.peek(); }

    public int GetNumPoints() { return _RoutePoints.size; }
}

