package com.mygdx.game;

public enum EEnemyData {
    MINION_A(1, 2, 0),
    MINION_B(1, 2, 1),
    MINION_C(1, 2, 3),

    LEADER_A(5, 1, 0),
    LEADER_B(5, 1, 1),
    LEADER_C(5, 1, 3),

    MINIBOSS_A(30, 0, 0),
    MINIBOSS_B(30, 0, 1),
    MINIBOSS_C(30, 0, 3),

    INVALID(-1, -1, -1);

    private EnemyData _EnemyData;

    EEnemyData(int LifePoints, int SpriteRow, int SpriteCol) {
        if (LifePoints == -1) {
            return;
        }

        _EnemyData = new EnemyData(LifePoints, SpriteRow, SpriteCol);
    }

    EnemyData Get() { return _EnemyData; }
}

class EnemyData extends SpriteData {
    private final int _LifePoints;
    EnemyData(int LifePoints, int SpriteRow, int SpriteCol) {
        super(SpriteRow, SpriteCol);
        _LifePoints = LifePoints;
    }

    public int GetLifePoints() { return _LifePoints; }
}
