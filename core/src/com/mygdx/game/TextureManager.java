package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.ObjectMap;

import java.util.concurrent.Semaphore;

public class TextureManager {
    private final ObjectMap<String, Texture> _TexturesLoaded = new ObjectMap<>();
    private final Semaphore _mutex = new Semaphore(1);
    private static TextureManager _TextureManager;

    private TextureManager() {}

    public static TextureManager Get() {
        if (_TextureManager == null) {
            _TextureManager = new TextureManager();
        }

        return _TextureManager;
    }

    public Texture GetTexture(String TextureLocation) {
        Texture TextureLoaded = null;
        try {
            TextureLoaded = _TexturesLoaded.get(TextureLocation);
            if (TextureLoaded == null) {
                _mutex.acquire();
                TextureLoaded = _TexturesLoaded.get(TextureLocation);

                if (TextureLoaded == null) {
                    TextureLoaded = new Texture(TextureLocation);
                    _TexturesLoaded.put(TextureLocation, TextureLoaded);
                }
            }
        } catch (InterruptedException e) {
            Gdx.app.error("TextureManager", "GetTexture: ", e);
        } finally {
            _mutex.release();
        }

        return TextureLoaded;
    }
}
