package com.mygdx.game;

public class SpriteData {
    private final int _SpriteRow;
    private final int _SpriteCol;

    public SpriteData(int SpriteRow, int SpriteCol) {
        _SpriteRow = SpriteRow;
        _SpriteCol = SpriteCol;
    }

    public int GetSpriteRow() { return _SpriteRow; }
    public int GetSpriteCol() { return _SpriteCol; }
}
