package com.mygdx.game.Input;

import com.badlogic.gdx.math.Vector2;

public class InputControllerState {
    private final Vector2 _DirectionalInput = new Vector2(0, 0);

    private boolean _bFire;
    private boolean _bPause;


    public void FlushControllerState() {
        _DirectionalInput.setZero();
        _bFire = false;
        _bPause = false;
    }

    public void MergeControllerState(final InputControllerState InputState) {
        _DirectionalInput.add(InputState.GetDirectionalInput());
        _DirectionalInput.set(_DirectionalInput.clamp(-1, 1));

        _bFire = _bFire || InputState.GetFireInput();
        _bPause = _bPause || InputState.GetPauseInput();
    }

    public void SetVerticalDirection(final float AxisX, final float AxisY) {
        _DirectionalInput.set(AxisX, AxisY);
    }

    public Vector2 GetDirectionalInput() {
        return _DirectionalInput;
    }

    public void SetFireInput(final boolean bFire) {
        _bFire = bFire;
    }

    public boolean GetFireInput() {
        return _bFire;
    }

    public void SetPauseInput(final boolean bPause) {
        _bPause = bPause;
    }

    public boolean GetPauseInput() {
        return _bPause;
    }

    public String ToString() { return "DPad" + _DirectionalInput.toString(); }
}
