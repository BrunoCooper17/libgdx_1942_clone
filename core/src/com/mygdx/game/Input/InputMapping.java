package com.mygdx.game.Input;

import com.badlogic.gdx.utils.Array;

public class InputMapping {
    private final Array<Integer> _InputCodes;

    public InputMapping(int Up, int Down, int Left, int Right, int Fire, int Pause) {
        _InputCodes = new Array<Integer>(EInputMap.MAX_INPUT.Get());
        _InputCodes.addAll(Up, Down, Left, Right, Fire, Pause);
    }

   public EInputMap GetInputMap(int InputCode) {
        int indexFound = _InputCodes.indexOf(InputCode, true);
        if( indexFound == -1) {
            return EInputMap.MAX_INPUT;
        }

        return EInputMap.values()[indexFound];
    }

    public int GetInputCode(EInputMap InputMap) {
        return _InputCodes.get(InputMap.Get());
    }
}
