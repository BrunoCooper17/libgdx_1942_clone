package com.mygdx.game.Input;

import com.badlogic.gdx.controllers.Controller;

public enum EGamepadMapping {
    A, B, X, Y,
    RB, RT, LB, LT, RSB, LSB,
    D_UP, D_DOWN, D_LEFT, D_RIGHT,
    START, SELECT;

    public int Get(Controller Control) {
        switch (this) {
            case A:
                return Control.getMapping().buttonA;
            case B:
                return Control.getMapping().buttonB;
            case X:
                return Control.getMapping().buttonX;
            case Y:
                return Control.getMapping().buttonY;
            case RB:
                return Control.getMapping().buttonR1;
            case RT:
                return Control.getMapping().buttonR2;
            case LB:
                return Control.getMapping().buttonL1;
            case LT:
                return Control.getMapping().buttonL2;
            case RSB:
                return Control.getMapping().buttonRightStick;
            case LSB:
                return Control.getMapping().buttonLeftStick;
            case D_UP:
                return Control.getMapping().buttonDpadUp;
            case D_DOWN:
                return Control.getMapping().buttonDpadDown;
            case D_LEFT:
                return Control.getMapping().buttonDpadLeft;
            case D_RIGHT:
                return Control.getMapping().buttonDpadRight;
            case START:
                return Control.getMapping().buttonStart;
            case SELECT:
                return Control.getMapping().buttonBack;
        }
        return -1;
    }
}
