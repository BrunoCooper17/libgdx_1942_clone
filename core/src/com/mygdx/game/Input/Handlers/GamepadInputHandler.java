package com.mygdx.game.Input.Handlers;

import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.Input.EGamepadMapping;
import com.mygdx.game.Input.EInputMap;
import com.mygdx.game.Input.InputMapping;

public class GamepadInputHandler extends InputHandler {
    public GamepadInputHandler(int InPlayerIndex, InputMapping InInputMapping) {
        super(InPlayerIndex, InInputMapping);
    }

    @Override
    public void SnapshotInputInController() {
        _ControlState.FlushControllerState();

        Array<Controller> Gamepads = Controllers.getControllers();
        if (Gamepads.size <= GetPlayerIndex()) {
            return;
        }

        Controller Gamepad = null;
        for (Controller PossibleGamepad : Gamepads) {
            if (PossibleGamepad.getPlayerIndex() == GetPlayerIndex()) {
               Gamepad = PossibleGamepad;
               break;
            }
        }

        if (Gamepad == null || !Gamepad.isConnected()) {
            return;
        }

        InputMapping Mapping = GetInputMapping();
        final int DPadX = (IsButtonPressed(Gamepad, Mapping, EInputMap.LEFT)? -1 : 0) +
                (IsButtonPressed(Gamepad, Mapping, EInputMap.RIGHT) ? 1 : 0);
        final int DPadY = (IsButtonPressed(Gamepad, Mapping, EInputMap.DOWN) ? -1 : 0) +
                (IsButtonPressed(Gamepad, Mapping, EInputMap.UP) ? 1 : 0);
        _ControlState.SetVerticalDirection(DPadX, DPadY);

        _ControlState.SetFireInput(IsButtonPressed(Gamepad, Mapping, EInputMap.FIRE));
        _ControlState.SetPauseInput(IsButtonPressed(Gamepad, Mapping, EInputMap.PAUSE));
    }

    boolean IsButtonPressed(Controller Gamepad, InputMapping Mapping, EInputMap InputMap) {
        return Gamepad.getButton(
                EGamepadMapping.values()[Mapping.GetInputCode(InputMap)]
                        .Get(Gamepad));
    }

    @Override
    public void Cleanup() {
    }

}
