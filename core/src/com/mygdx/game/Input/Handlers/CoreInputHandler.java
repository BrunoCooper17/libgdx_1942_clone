package com.mygdx.game.Input.Handlers;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.Input.EInputMap;
import com.mygdx.game.Input.InputMapping;

public class CoreInputHandler extends InputHandler {
    public CoreInputHandler(int InPlayerIndex, InputMapping InInputMapping) {
        super(InPlayerIndex, InInputMapping);
    }

    @Override
    public void SnapshotInputInController() {
        _ControlState.FlushControllerState();

        InputMapping ControlMapping = GetInputMapping();
        final int AxisX = (Gdx.input.isKeyPressed(ControlMapping.GetInputCode(EInputMap.LEFT)) ? -1 : 0)
                + (Gdx.input.isKeyPressed(ControlMapping.GetInputCode(EInputMap.RIGHT)) ? 1 : 0);
        final int AxisY = (Gdx.input.isKeyPressed(ControlMapping.GetInputCode(EInputMap.DOWN)) ? -1 : 0)
                + (Gdx.input.isKeyPressed(ControlMapping.GetInputCode(EInputMap.UP)) ? 1 : 0);
        _ControlState.SetVerticalDirection(AxisX, AxisY);

        _ControlState.SetFireInput(Gdx.input.isKeyPressed(ControlMapping.GetInputCode(EInputMap.FIRE)));
        _ControlState.SetPauseInput(Gdx.input.isKeyPressed(ControlMapping.GetInputCode(EInputMap.PAUSE)));
    }

    @Override
    public void Cleanup() {
    }
}
