package com.mygdx.game.Input.Handlers;

import com.mygdx.game.Input.InputControllerState;
import com.mygdx.game.Input.InputMapping;

public abstract class InputHandler {
    private final int _PlayerIndex;
    private final InputMapping _InputMapping;
    protected final InputControllerState _ControlState = new InputControllerState();


    public InputHandler(int InPlayerIndex, InputMapping InInputMapping) {
        _PlayerIndex = InPlayerIndex;
        _InputMapping = InInputMapping;
    }

    public int GetPlayerIndex() { return _PlayerIndex; }
    public InputMapping GetInputMapping() { return _InputMapping; }

    public InputControllerState GetInputControllerState() {
        return _ControlState;
    }

    public abstract void SnapshotInputInController();

    public abstract void Cleanup();
}
