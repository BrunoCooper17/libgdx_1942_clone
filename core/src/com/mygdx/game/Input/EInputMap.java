package com.mygdx.game.Input;

public enum EInputMap {
    UP, DOWN, LEFT, RIGHT,
    FIRE, PAUSE,
    MAX_INPUT;

    public int Get() { return this.ordinal(); }
}
