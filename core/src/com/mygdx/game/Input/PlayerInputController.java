package com.mygdx.game.Input;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.Input.Handlers.CoreInputHandler;
import com.mygdx.game.Input.Handlers.GamepadInputHandler;

public class PlayerInputController {
    private final CoreInputHandler _CoreInputHandler;
    private final GamepadInputHandler _GamepadInputHandler;
    private final int _PlayerIndex;

    private final InputControllerState _ControlState = new InputControllerState();


    public PlayerInputController(int PlayerIndex, InputMapping KbdMapping, InputMapping GamepadMapping) {
        _PlayerIndex = PlayerIndex;

        if (KbdMapping != null) {
            _CoreInputHandler = new CoreInputHandler(_PlayerIndex, KbdMapping);
        }
        else {
            _CoreInputHandler = null;
        }

        if(GamepadMapping != null) {
            _GamepadInputHandler = new GamepadInputHandler(_PlayerIndex, GamepadMapping);
        }
        else {
            _GamepadInputHandler = null;
        }
    }

    public int GetPlayerIndex() { return _PlayerIndex; }

    public InputControllerState GetControllerInputState() {
        return _ControlState;
    }

    public void UpdateInputHandlers() {
        _ControlState.FlushControllerState();

        if (_CoreInputHandler != null) {
            _CoreInputHandler.SnapshotInputInController();
            _ControlState.MergeControllerState(_CoreInputHandler.GetInputControllerState());
        }

        if (_GamepadInputHandler != null) {
            _GamepadInputHandler.SnapshotInputInController();
            _ControlState.MergeControllerState(_GamepadInputHandler.GetInputControllerState());
        }
    }

    protected void Cleanup() {
        if (_CoreInputHandler != null) {
            _CoreInputHandler.Cleanup();
        }

        if (_GamepadInputHandler != null) {
            _GamepadInputHandler.Cleanup();
        }
    }
}
