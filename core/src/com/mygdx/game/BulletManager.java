package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

public class BulletManager extends GameActor {
    private final Array<Bullet> _BulletPool;

    BulletManager(int MaxAmountBullets) {
        _BulletPool = new Array<Bullet>(MaxAmountBullets);
        for (int Num=0; Num < MaxAmountBullets; ++Num) {
            _BulletPool.add(new Bullet());
        }
    }

    public Bullet GetBullet() {
        for (Bullet bullet: _BulletPool) {
            if (!bullet.IsActive()) {
                bullet.SetActive(true);
                return bullet;
            }
        }

        return null;
    }

    @Override
    public void Update(float DeltaTime) {
        for (Bullet bullet: _BulletPool) {
            if(bullet.IsActive()) {
                bullet.Update(DeltaTime);
            }
        }
    }

    @Override
    public void Render(SpriteBatch Batch) {
        for (Bullet bullet: _BulletPool) {
            if(bullet.IsActive()) {
                bullet.Render(Batch);
            }
        }
    }
}
