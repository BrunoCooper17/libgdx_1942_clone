package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class Bullet extends GameActor {
    private final Sprite _Sprite;
    private final float _SpriteMaxLength;
    private final Vector2 _Direction = new Vector2(0, 0);
    final int _SpriteSize = 16;

    private boolean _bActive = false;
    private BulletData _BulletData = null;

    Bullet() {
        // Small optimization: Store textures inside a Map manager, so they are created/loaded only once?
        Texture textureTilemap = TextureManager.Get().
                GetTexture("kenney_pixel-shmup/Tilemap/tiles_packed.png");
        _Sprite = new Sprite(textureTilemap, 0, 0, _SpriteSize, _SpriteSize);
        _Sprite.setOriginCenter();

        _SpriteMaxLength = (int) Math.sqrt(2 * _SpriteSize);
    }

    void SetDirection(float DirX, float DirY) {
        _Direction.set(DirX, DirY);
        _Direction.nor();

        _Transform.setRotation(_Direction.angleRad());
    }

    public boolean IsActive() { return _bActive; }
    public void SetActive(boolean bActive) { _bActive = bActive; }

    public void SetPosition(BulletData InBulletData, float X, float Y) {
        Vector2 Position = _Transform.getPosition();
        Position.set(X, Y);
        _Transform.setPosition(Position);

        _BulletData = InBulletData;
        _Sprite.setRegion(_BulletData.GetSpriteCol() * _SpriteSize,
                _BulletData.GetSpriteRow() * _SpriteSize,
                _SpriteSize, _SpriteSize);
    }

    @Override
    public void Update(float DeltaTime) {
        float MaxSpeed = 0f;
        if (_BulletData != null) {
            MaxSpeed = _BulletData.GetMaxSpeed();
        }

        if (MaxSpeed < 0.01f || _Direction.isZero(0.01f)) {
            _bActive = false;
            return;
        }

        Vector2 Position = _Transform.getPosition();

        final float CacheDeltaSpeed = DeltaTime * MaxSpeed;
        Position.add(CacheDeltaSpeed * _Direction.x, CacheDeltaSpeed * _Direction.y);
        if ((Math.abs(Position.x) - _SpriteMaxLength) > 160
                || (Math.abs(Position.y) - _SpriteMaxLength) > 90) {
           _bActive = false;
        }

        _Transform.setPosition(Position);
    }

    @Override
    public void Render(SpriteBatch Batch) {
        Vector2 Position = _Transform.getPosition();
        _Sprite.setOriginBasedPosition(Position.x, Position.y);
        _Sprite.setRotation(MathUtils.radiansToDegrees * _Transform.getRotation() - 90f);
        _Sprite.draw(Batch);
    }
}
