package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Transform;

public class GameActor {
    protected Transform _Transform;
    protected Vector2 _Scale;

    public GameActor() {
        _Transform = new Transform();
        _Scale = new Vector2(1, 1);
    }

    // Loop functions
    public void Update(final float DeltaTime) {}
    public void Render(SpriteBatch Batch) {}
}
