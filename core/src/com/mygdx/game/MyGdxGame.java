package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.ScreenUtils;

public class MyGdxGame extends ApplicationAdapter {
    SpriteBatch _Batch;
    OrthographicCamera _Camera;

    Player _Player;
    Background _Background;

    EnemyManager _EnemyManager;

    @Override
    public void create() {
        // Preload Textures
        TextureManager.Get().GetTexture("kenney_pixel-shmup/Tilemap/Map01.png");
        TextureManager.Get().GetTexture("kenney_pixel-shmup/Tilemap/ships_packed.png");
        TextureManager.Get().GetTexture("kenney_pixel-shmup/Tilemap/tiles_packed.png");

        _Camera = new OrthographicCamera(320, 180);
        _Batch = new SpriteBatch();
        _Background = new Background("kenney_pixel-shmup/Tilemap/Map01.png", 15f);

        _Player = new Player(0);
        _EnemyManager = new EnemyManager(50);

        _EnemyManager.Init("LevelPatterns/01.txt");
    }

    @Override
    public void render() {
        ScreenUtils.clear(1f, 1f, 1f, 1f);

        final float DeltaTime = Gdx.graphics.getDeltaTime();
        _Player.Update(DeltaTime);
        _EnemyManager.Update(DeltaTime);
        _Background.Update(DeltaTime);

        _Camera.update();
        _Batch.setProjectionMatrix(_Camera.combined);

        _Batch.begin();
        _Background.Render(_Batch);
        _Player.Render(_Batch);
        _EnemyManager.Render(_Batch);
        _Batch.end();
    }

    @Override
    public void dispose() {
        _Batch.dispose();
    }
}
