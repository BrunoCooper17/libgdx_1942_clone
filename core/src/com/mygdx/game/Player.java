package com.mygdx.game;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Input.EGamepadMapping;
import com.mygdx.game.Input.InputControllerState;
import com.mygdx.game.Input.InputMapping;
import com.mygdx.game.Input.PlayerInputController;

public class Player extends GameActor {
    private Sprite _ShipSprite = null;

    final private float _MaxSpeed = 120.f;

    private PlayerInputController _PlayerInput = null;
    private final BulletManager _BulletManager;


    private static class AutoFireController {
        private final float _AutoFireTime;
        private float _AutoFireTimer = 0f;

        AutoFireController(final float AutoFireTime) {
            _AutoFireTimer = _AutoFireTime = AutoFireTime;
        }

        boolean FireEvent(InputControllerState ControllerState, final float DeltaTime) {
            if (!ControllerState.GetFireInput()) {
                _AutoFireTimer = _AutoFireTime;
                return false;
            }

            _AutoFireTimer += DeltaTime;
            if (_AutoFireTimer > _AutoFireTime) {
                _AutoFireTimer -= _AutoFireTime;
                return true;
            }

            return false;
        }
    }

    private final AutoFireController _AutoFireController = new AutoFireController(.2f);


    public Player(int PlayerIndex) {
        super();

        final int SpriteSize = 32;
        final int SpriteRow = 2;
        final int SpriteCol = 2;

        Texture textureTilemap = TextureManager.Get().
                GetTexture("kenney_pixel-shmup/Tilemap/ships_packed.png");
        _ShipSprite = new Sprite(textureTilemap,
                SpriteCol * SpriteSize, SpriteRow * SpriteSize,
                SpriteSize, SpriteSize);
        _ShipSprite.setOriginCenter();

        _PlayerInput = new PlayerInputController(
                0,
                new InputMapping(Input.Keys.UP, Input.Keys.DOWN, Input.Keys.LEFT, Input.Keys.RIGHT,
                        Input.Keys.SPACE, Input.Keys.ENTER),
                new InputMapping(EGamepadMapping.D_UP.ordinal(), EGamepadMapping.D_DOWN.ordinal(),
                        EGamepadMapping.D_LEFT.ordinal(), EGamepadMapping.D_RIGHT.ordinal(),
                        EGamepadMapping.A.ordinal(), EGamepadMapping.START.ordinal()));

        _BulletManager = new BulletManager(15);
    }

    private void ProcessMovement(final Vector2 InputDirection, final float DeltaTime) {
        Vector2 CurrentPosition = _Transform.getPosition();
        CurrentPosition.x += InputDirection.x * _MaxSpeed * DeltaTime;
        CurrentPosition.y += InputDirection.y * _MaxSpeed * DeltaTime;

        // Don't let player go outside the screen (320x180 but 16:9)
        CurrentPosition.x = MathUtils.clamp(CurrentPosition.x, -160.f, 160.f);
        CurrentPosition.y = MathUtils.clamp(CurrentPosition.y, -90.f, 90.f);

        _Transform.setPosition(CurrentPosition);
    }

    private void ProcessFire(InputControllerState ControllerState, final float DeltaTime) {
        if (!_AutoFireController.FireEvent(ControllerState, DeltaTime)) {
            return;
        }

        Bullet NewBullet = _BulletManager.GetBullet();

        if (NewBullet != null) {
            Vector2 Position = _Transform.getPosition();
            NewBullet.SetDirection(0f, 1f);
            NewBullet.SetPosition(EBulletData.BULLET_1.Get(), Position.x, Position.y);
        }
    }

    @Override
    public void Update(final float DeltaTime) {
        _PlayerInput.UpdateInputHandlers();
        InputControllerState ControllerState = _PlayerInput.GetControllerInputState();

        ProcessMovement(ControllerState.GetDirectionalInput(), DeltaTime);
        ProcessFire(ControllerState, DeltaTime);

        _BulletManager.Update(DeltaTime);
    }

    @Override
    public void Render(SpriteBatch Batch) {
        _BulletManager.Render(Batch);

        Vector2 Position = _Transform.getPosition();
        _ShipSprite.setOriginBasedPosition(Position.x, Position.y);
        _ShipSprite.draw(Batch);
    }
}
